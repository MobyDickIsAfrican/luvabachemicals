from django.contrib import admin
from .models import Category, Product, Article, Quote, Enquiry
# Register your models here.

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Article)
admin.site.register(Quote)
admin.site.register(Enquiry)

admin.site.site_header = 'My Site Admin'
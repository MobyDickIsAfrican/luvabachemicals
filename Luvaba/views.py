from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework.parsers import JSONParser
from django.conf import settings

from .models import Category, Product, Article, Quote, Enquiry
from .serializers import *
from django.views.generic import View
import os


@api_view(['GET',])
def getProducts(request):
    cats = Category.objects.all()
    pyArray = []
    for c in cats:
        products = c.products.all()
        serialP = ProductSerializer(products, many = True)
        listItem = {'cat': c.category, 'products': serialP.data}
        pyArray.append(listItem)
    return JsonResponse(pyArray, safe = False)

@api_view(['GET',])
def getArticles(request):
    articles = Article.objects.all()
    serialArticles = ArticleSerializer(articles, context = {'request': request}, many = True)
    return Response({'data': serialArticles.data})

@api_view(['GET',])
def getArticle(request, pk):
    item = get_object_or_404(Article, id = pk)
    serial = ArticleSerializer(item, context = {'request': request})
    return Response({'data': serial.data})

@api_view(['POST',])
def Order(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        serializer = QuoteSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status = 201)
        else:
            return JsonResponse(serializer.errors, status = 400)

@api_view(['POST',])
def Enquire(request):
    if request.method == "POST":
        data = JSONParser().parse(request)
        serializer = EnquirySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status = 201)
        else:
            return JsonResponse(serializer.errors, status = 400)

class FrontendAppView(View):
    index_file_path = os.path.join(settings.BASE_DIR, 'build', 'index.html')
    def get(self, request):
        try:
            with open(self.index_file_path) as f:
                return HttpResponse(f.read())
        except FileNotFoundError:
            return HttpResponse('Oops', status=501)
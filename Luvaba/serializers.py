from rest_framework import serializers
from .models import Category, Product, Article, Quote, Enquiry

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('pk', 'category')

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('pk', 'name', 'description', 'image', 'imageDescription')

class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('pk', 'title', 'subHeading', 'description', 'imageUpload', 'imageExists')

class QuoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quote
        fields = ('pk', 'size', 'product', 'email', 'number', 'fullName')

class EnquirySerializer(serializers.ModelSerializer):
    class Meta:
        model = Enquiry
        fields = ('pk', 'message', 'email', 'number', 'fullName')
from django.db import models

# Create your models here.

class Category(models.Model):
    category = models.CharField("First Name", max_length = 50)
    created = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.category

class Product(models.Model):
    category = models.ForeignKey(Category, related_name = "products", on_delete = models.CASCADE, null = True)
    name = models.CharField("name of product",  max_length = 50, blank = False)
    description = models.TextField(max_length = 800)
    image = models.ImageField(upload_to = 'media/Product_image', null = True)
    imageDescription = models.TextField("Describe Image", max_length = 50)

    def __str__(self):
        return self.name

class Article(models.Model):
    title = models.CharField("Title", max_length = 50)
    subHeading = models.CharField("Sub-Heading", max_length = 70)
    description = models.TextField("Article Text", max_length = 10000)
    imageUpload = models.ImageField(upload_to = 'media/Article_image', null = True)
    imageExists = models.BooleanField("Did You Upload Image", default = False)

    def __str__(self):
        return self.title

#{size, fullName, email, tel}
class Quote(models.Model):
    size = models.CharField("Size", max_length = 7)
    product = models.CharField("Product", max_length = 50)
    email = models.EmailField("Email", max_length = 100)
    number = models.CharField("Cell", max_length = 15)
    fullName = models.CharField("Full Name", max_length = 100)

    def __str__(self):
        return self.fullName

#{fullName, email, cell, message}
class Enquiry(models.Model):
    fullName = models.CharField("Full Name", max_length = 100)
    email = models.EmailField("Email", max_length = 100)
    number = models.CharField("Cell", max_length = 15)
    message = models.TextField("Message", max_length = 600)

    def __str__(self):
        return self.fullName
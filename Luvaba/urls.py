from django.urls import path
from .views import getProducts, getArticles, getArticle, Order, Enquire
from django.conf import settings
from django.conf.urls.static import static
import Luvaba.views as views

urlpatterns = [
    path('api/products', getProducts),
    path('api/articles', getArticles),
    path('api/article/<int:pk>', getArticle),
    path('api/post/quote', Order),
    path('api/post/enquiry', Enquire),
    path('', views.FrontendAppView.as_view()),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
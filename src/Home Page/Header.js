import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './header.css';
import { FaBars } from "react-icons/fa";
import { FaTimes } from "react-icons/fa";

class Header extends Component{
    constructor(props){
        super(props);
        this.menuClick = this.menuClick.bind(this);
        this.bar = (<FaBars style={{color: "rgb(1, 1, 41)"}} size="2rem"/>);
        this.times = (<FaTimes style={{color: "rgb(1, 1, 41)"}} size="2rem"/>);
        this.state = {show: "none", icon: this.bar};
    };
    menuClick(e){
        return this.toggleMenu();
    }
    //function for toggling the menu block between "none" and "block"
    toggleMenu(){
        if(this.state.show === "none"){
            return this.setState({show: "block", icon: this.times})
        } else{
            return this.setState({show: "none", icon: this.bar})
        }
    }
    render(){
        let navLinks = (
            <div className="nav-links" style={{display: this.state.show}}>
                        <div className="about-link">
                            <Link to="/about">
                                <span>
                                    About
                                </span>
                            </Link>
                        </div>
                        <div className="products-link">
                            <Link to="/products">
                                <span>
                                    Products
                                </span>
                            </Link>
                        </div>
                        <div className="blog-link">
                            <Link to="/blog">
                                <span>
                                    Blog
                                </span>
                            </Link>
                        </div>
                        <div className="contact-link">
                            <Link to="/contact">
                                <span>
                                    Contact Us
                                </span>
                            </Link>
                        </div>
            </div>
        )
        return (
            <div className="header-container">
                <nav className="nav-bar">
                    <div className="header-row">
                        <div className="header-image-container">    
                            <Link to="/">
                            <img src="https://luvabastatic.s3.eu-west-2.amazonaws.com/home-images/icon+2.png" 
                            alt="Luvaba company logo" className="header-image"></img>
                            </Link>              
                        </div>
                        <div className="about-link">
                            <Link to="/about">
                                <span>
                                    About
                                </span>
                            </Link>
                        </div>
                        <div className="products-link">
                            <Link to="/products">
                                <span>
                                    Products
                                </span>
                            </Link>
                        </div>
                        <div className="blog-link">
                            <Link to="/blog">
                                <span>
                                    Blog
                                </span>
                            </Link>
                        </div>
                        <div className="contact-link">
                            <Link to="/contact">
                                <span>
                                    Contact Us
                                </span>
                            </Link>
                        </div>
                    </div>
                    <div className="nav-header">
                        <div className="header-image-container-1">    
                            <Link to="/">
                                <img src="https://luvabastatic.s3.eu-west-2.amazonaws.com/home-images/icon+2.png" 
                                alt="company logo" className="header-image"></img>
                            </Link>              
                        </div>
                        <div className="menu-icon" >
                            <div className="menu-icon-wrapper">
                                <span className="menu-icon-span" onClick={this.menuClick}>
                                    {this.state.icon}
                                </span>
                            </div>
                            {navLinks}
                        </div>
                    </div>
                </nav>
                              
            </div>
        )
    }
};

export default Header;
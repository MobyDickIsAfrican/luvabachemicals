import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './CallToAction.css';

class CallToAction extends Component{
    render(){
        return (
            <div className="get-quote-container">
                <div className="get-quote-div">
                    <span>
                        Get Free Quote Today
                    </span>
                </div>
                <div className="get-quote-button">
                    <Link to="/products">
                        <button type="button">Get Quote</button>
                    </Link>
                </div>
            </div>
        )
    }
};

export default CallToAction;
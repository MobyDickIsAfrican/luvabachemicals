import React, {Component} from 'react';
import './ProductCat.css';

class ProductCat extends Component{
    render(){
        let {title, src, alt, desc} = this.props.info;
        return (
            <div className="product-cat-image-container">
                <ul>
                    <li className="cat-title">
                        {title}
                    </li>
                    <li className="cat-image">
                        <img src={src} alt={alt}></img>
                    </li>
                    <li className="cat-desc">
                        {desc}
                    </li>
                </ul>
            </div>
        )
    }
};

export default ProductCat;
import React, {Component} from 'react';
import ProductCat from './ProductCat.js';
import './MediaContent.css';

class MediaContent extends Component{
    render(){
        let data = [{title: "Personal Care", src: "https://luvabastatic.s3.eu-west-2.amazonaws.com/home-images/personalCare.jpg",
        alt: "personal care products such as hand sanitisers and antiseptic",
         desc: "Hand Soap, Hand Sanitiser, Body Soap aand Antiseptic"}, 
         {title: "Homecare", src: "https://luvabastatic.s3.eu-west-2.amazonaws.com/home-images/home+care.jpg",
         alt: "home care products such as Bleach, Dishwashing Liquid, Drain Cleaner",
          desc: "Bleach, Dishwashing Liquid, Drain Cleaner, Surface Disinfectant, Laundry Powder Detergent and Fabric Softners" },
          {title: "Industrial Care", src: "https://luvabastatic.s3.eu-west-2.amazonaws.com/home-images/car+cleaner.jpg",
          alt: "industrial care products such as Floor Cleaner, High Performance Degreaser, Car Shmpoo, Tyre and Dash Shine",
           desc: "Floor Cleaner, High Performance Degreaser, Car Shmpoo, Tyre and Dash Shine"}
        ];
        const data1 = data[0];
        let data2 = data[1]; 
        let data3 = data[2]; 

        return (
            <div className="media-content-container">
                <ProductCat info={data1}/>
                <ProductCat info={data2}/>
                <ProductCat info={data3}/>
            </div>
        )
    }
};

export default MediaContent;
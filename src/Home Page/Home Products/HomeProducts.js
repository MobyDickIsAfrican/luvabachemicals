import React, {Component} from 'react';
import MediaContent from './MediaContent.js';
import {Link} from 'react-router-dom';
import './HomeProducts.css'

class HomeProducts extends Component{
    render(){
        return (
            <div className="our-products-container">
                <div className="our-products-title-div">
                    <span className="our-products-title-span">Our Products</span>
                </div>
                <div className="our-products-wrapper">
                    <div className="our-products-content"> 
                        <MediaContent />
                        <div className="view-products-button">
                            <Link to="/products">
                                <button type="button" value="View Products"> View Products</button>
                            </Link>
                        </div>
                        <div className="our-products-para-container">
                            <p>
                            The purpose of the business is to develop, manufacture, package and sell hygiene and cleaning chemicals
                             to the community in which it operates and to government, retail and industrial establishments around
                              Eastern Cape province. The products produced by the business are clustered into three categories. Personal
                               care products: liquid hand soap, liquid body soap, hand sanitiser, lubricant and antiseptic liquid. Homecare
                                products: dishwashing liquid, stain/soap scum remover, bleach, drain cleaner, surface disinfectant, carpet
                                 cleaner, fabric softener and laundry powder detergent. Industrial and automotive products: high performance
                                  degreaser, floor cleaner, car body soap and tyre and dash shine
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
};

export default HomeProducts;
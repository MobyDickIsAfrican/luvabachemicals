import React, {Component} from 'react';
import './OurFounderContainer.css';

class OurFounderContainer extends Component{
    render(){
        return (
            <div className="our-founder-container">
                <div className="our-founder-title">
                    <span>Our Founder</span>
                </div>
                <div className="our-founder-content-container">
                    <div className="meet-founder-media-container">
                        <ul>
                            <li className="founder-image-li">
                                <img src="https://luvabastatic.s3.eu-west-2.amazonaws.com/home-images/circle-cropped.png" 
                                alt="Luvo Ntsangani" className="founder-image"></img>
                            </li>
                            <li className="founder-media-li">
                                <div className="founder-media-wrapper">
                                    <ul className="labels-ul">
                                        <li className="meet-founder-name">
                                            <span>
                                                Luvo Ntsangani
                                            </span>
                                        </li>
                                        <li className="meet-founder-label">
                                            <span>
                                                Managing Director and Founder
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div className="meet-founder-para">
                        <p>
                        "The business was established on the 30th May 2020. With our normal day to day life
                         activities as human beings we tend to be much at risk of contracting diseases that
                          are caused by viruses, germs and bacteria that thrive in unhygienic and dirty environments.
                           A business has been born out of the pain of being part and seeing the poor communities of
                            the Eastern Cape and South Africa at large struggle to enjoy their livelihoods due to the
                             fact that they do not have access and/or cannot afford good quality hygiene and cleaning
                              chemical products for their personal use, homecare and valuable assets."
                        </p>
                    </div>
                </div>
            </div>
        )
    }
};

export default OurFounderContainer;
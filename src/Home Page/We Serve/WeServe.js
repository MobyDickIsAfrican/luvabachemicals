import React, {Component} from 'react';
import { FaUserFriends } from "react-icons/fa";
import { FaFlask } from "react-icons/fa";
import { MdAccountBalance } from "react-icons/md";
import { MdShoppingCart  } from "react-icons/md";
import CustomerIcon from './CustomerIcon.js';
import './WeServe.css';

class WeServeContainer extends Component{
    createIcons(){
        let icons = [{icon: (
                <FaUserFriends style={{color: "lightblue"}} size="5rem" />), title: "Communities"},
                {icon: (
                    <MdAccountBalance style={{color: "lightblue"}} size="5rem" />), title: "Government"}, 
                    {icon: (
                        <MdShoppingCart style={{color: "lightblue"}} size="5rem" />), title: "Retail"}, 
                        {icon: (
                            <FaFlask style={{color: "lightblue"}} size="5rem" />), title: "Industrial"}];
        let list = [];
        icons.forEach((item, _index) => {
            let element = (<CustomerIcon icon={item.icon} title={item.title} />)
            list.push(element);
        });
        return list;

    }
    render(){
        const list = this.createIcons();
        return (
            <div className="we-serve-customer-wrapper">
                <div className="we-serve-title">
                    <span>We Serve</span>
                </div>
                <div className="we-serve-customer-container">
                    <ul>
                        {list}
                    </ul>
                </div>
            </div>
        )
    }
};

export default WeServeContainer;
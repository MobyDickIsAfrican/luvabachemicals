import React, {Component} from 'react';
import './CustomerIcon.css';

class CustomerIcon extends Component{
    render(){
        const title = this.props.title;
        const icon = this.props.icon;
        return (
            <div className="we-serve-customer">
                <div>
                    <li className="we-serve-icon-li">
                        {icon}
                    </li>
                    <li className="we-serve-title-li">
                        <span>{title}</span>
                    </li>
                </div>
            </div>
        )
    }
};

export default CustomerIcon;
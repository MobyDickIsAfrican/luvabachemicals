import React, {Component} from 'react';
import './HomeOne.css'

class HomeOne extends Component{
    render(){
        return (
            <div className="home-one-container">
                <div className="home-one-text-container">
                    <p>
                        Quality Hygiene and Cleaning Chemical Products For Personal Use, Homecare, Industrial Care and
                        Valuable Assets
                    </p>
                </div>
            </div>
        )
    }
};

export default HomeOne;
import React, {Component} from 'react';
import './HomeTwo.css'

class HomeTwo extends Component{
    render(){
        return (
            <div className="home-two-container">
                <div className="home-two-text">
                    <ul> 
                        <li className="home-two-title">
                            Who We Are
                        </li>
                        <li className="home-two-content">
                               Luvaba Chemical Solutions trading as Luvaba is a hygiene and cleaning
                               chemicals business situated in a small town called Whittlesea in the
                               Eastern Cape province of South Africa. The business is owned by Luvo
                               Ntangani who is an experienced product development scientist with a
                               Master of Science degree in Chemistry from the University of The
                               Witwatersrand coupled with 10 years working experience in the manufacturing
                               industry. Luvo has exceptional experience in developing, validating, 
                               comercialising hygiene and cleaning chemicals, as he has worked for big
                               companies like Unilever, Henkel and AECI.
                        </li>
                    </ul>
                </div>

            </div>
        )
    }
};

export default HomeTwo;
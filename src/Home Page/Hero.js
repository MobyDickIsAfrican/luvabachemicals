import React, {Component} from 'react';
import './Hero.css'


class Hero extends Component{
    constructor(props){
        super(props);
        this.state = {show: false};
        this.displayMessage = this.displayMessage.bind(this);
    };
    componentDidMount(){
        console.log(1);
        setTimeout(this.displayMessage, 1000);
    };
    displayMessage(){
        console.log(3);
        return this.setState({show: true})
    }
    render(){
    let msg = "";
        if(this.state.show){
            console.log(2);
            msg = "Quality Hygiene at Affordable Prices";
        }
        return (
            <div className="hero-container">
                <div className="image-container">
                    <span> {msg} </span>
                </div>
            </div>
        )
    }
};

export default Hero;
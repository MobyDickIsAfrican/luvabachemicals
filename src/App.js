import React, {Component} from 'react';
import './App.css';
import Header from './Home Page/Header.js';
import Footer from './Footer.js';
import { FaFacebookSquare } from "react-icons/fa";
import { FaLinkedin } from "react-icons/fa";
import { FaTwitterSquare } from "react-icons/fa";
import {BrowserRouter as Router, Route, Link, Switch} from 'react-router-dom';
import Home from './Home.js';
import ProductPanel from './Product Page/ProductPanel';
import BlogContainer from './Blog Page/BlogContainer.js';
import Article from './Blog Page/Article.js';
import Contact from './Contact/Contact.js';
import About from './About.js';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {showlinks: true} 
  };
  createSocial(){
    return (
      <div className="social-wrapper">
        <div className="social-link">
          <FaFacebookSquare size="2rem" style={{backgroundColor: "blue"}} />
        </div>
        <div className="social-link">
          <FaLinkedin size="2rem" style={{backgroundColor: "blue"}}/>
        </div>
        <div className="social-link">
          <FaTwitterSquare size="2rem" style={{backgroundColor: "blue"}}/>
        </div>
      </div>
    )
  }
  render(){
    let links = null;
      if(this.state.showlinks){
        links = this.createSocial();
      };
    return (
      <div className="App">
        <Router>
        <Header />
          <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/products" exact component={ProductPanel}/>
            <Route path="/blog" exact component={BlogContainer}/>
            <Route path="/blog/article/:article/:id" exact component={Article}/>
            <Route path="/contact" exact component={Contact}/>
            <Route path="/about" exact component={About}/>
          </Switch>
          <Footer links={links} />
        </Router>
        
      </div>
    )
  }
  }


export default App;
import React, {Component} from 'react';
import ProductFigure from './ProductFigure.js';
import './ProductCat.css';

class ProductCat extends Component{
    constructor(props){
        super(props);
        this.products = [{name: "x", desc: "lovely aroma", src: "home-images/circle-cropped.png", alt: "", id: 1}, 
        {name: "x", desc: "lovely aroma", src: "home-images/circle-cropped.png", alt: "", id: 2}, 
        {name: "x", desc: "lovely aroma", src: "home-images/circle-cropped.png", alt: "", id: 3}]
    };
    displayer(){
        let products = []
        this.props.products.forEach((p, _i) => {
            products.push(
                (<ProductFigure info={p} key={p.pk}/>)
            )
        });
        const title = this.props.title;
        return (
            <div className="product-list-div">
                <div className="cat-title-div">
                    <div className="cat-title-wrapper">
                        <span>{title}</span>
                    </div>
                </div>
                <div className="product-list">
                    <ul className="product-list-ul">
                        {products}
                    </ul>
                </div>
            </div>
        )

    }
    render(){
        const productList = this.displayer();
        return(
            <div className="product-cat-title-container">
                <div className="product-list">
                    {productList}
                </div>
            </div>
        )
    }
}

export default ProductCat;
import React, {Component} from 'react';
import './GetQuote.css';                                   

class GetQuote extends Component{
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    };
    handleSubmit(e){
        e.preventDefault();
        let sizeTarget = e.target.chooseSize;
        console.log(sizeTarget.selectedIndex);
        const size = sizeTarget.options[sizeTarget.selectedIndex].value;
        const fullName = e.target.fullName.value;
        const email = e.target.email.value;
        const number = e.target.tel.value;
        return this.props.handleSubmit({size, fullName, email, number});
    }
    render(){
     return (
         <div className="get-quote-container">
             <form method="POST" onSubmit={this.handleSubmit}>
                 <div className="size-dropdown">
                     <ul>
                         <li className="label">
                             <span>Size</span>
                         </li>
                         <li className="input-wrapper">
                             <select name="chooseSize" className="pick-size">
                                 <option value="750ml">750 mL</option>
                                 <option value="5L" selected> 5 L</option>
                                 <option value="25L">25 L</option>
                             </select>
                         </li>
                     </ul>
                 </div>
                 <div className="full-name-container">
                     <ul>
                         <li className="label">
                             <span>Full Name</span>
                         </li>
                         <li className="input-wrapper">
                             <input type="text" name="fullName" className="full-name" placeholder="John Doe"></input>
                         </li>
                     </ul>
                 </div>
                 <div className="email-container">
                     <ul>
                         <li className="label">
                             <span>Email</span>
                         </li>
                         <li className="input-wrapper">
                             <input type="email" name="email" className="email-input" placeholder="vuyo@company.co.za"></input>
                         </li>
                     </ul>
                 </div>
                 <div className="tel-container">
                     <ul>
                         <li className="label">
                             <span>Tel</span>
                         </li>
                         <li className="input-wrapper">
                             <input type="text" name="tel" className="tel-input"></input>
                         </li>
                     </ul>
                 </div>
                 <div className="button-div">
                    <button type="submit">Finish</button>
                </div>
             </form>
         </div>
     )
    }
}

export default GetQuote;
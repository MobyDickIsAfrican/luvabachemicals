import React, {Component} from 'react';
import ProductCat from './ProductCat.js';
import './ProductPanel.css';

class ProductPanel extends Component{
    constructor(props){
        super(props);
        this.state = {categories: []}
        this.categories = [{products: [{name: "x", desc: "lovely aroma", src: "home-images/personalCare.jpg", alt: "", id: 1}, 
        {name: "x", desc: "lovely aroma", src: "home-images/personalCare.jpg", alt: "", id: 2}, 
        {name: "x", desc: "lovely aroma", src: "home-images/personalCare.jpg", alt: "", id: 3}], cat: "Personal Care", id: 1}, 
        {products: [{name: "x", desc: "lovely aroma", src: "home-images/personalCare.jpg", alt: "", id: 1}, 
        {name: "x", desc: "lovely aroma", src: "home-images/personalCare.jpg", alt: "", id: 2}, 
        {name: "x", desc: "lovely aroma", src: "home-images/personalCare.jpg", alt: "", id: 3}], cat: "Home Care", id: 2},
        {products: [{name: "x", desc: "lovely aroma", src: "home-images/personalCare.jpg", alt: "", id: 1}, 
        {name: "x", desc: "lovely aroma", src: "home-images/personalCare.jpg", alt: "", id: 2}, 
        {name: "x", desc: "lovely aroma", src: "home-images/personalCare.jpg", alt: "", id: 3}], cat: "Industrial Care", id: 3}
    ]
    };
    componentDidMount(){
        fetch('https://luvabachemicals.herokuapp.com/api/products').then(res => res.json())
        .then(data => {
            console.log(data);
            this.setState({categories: data})
        })
    }
    displayer(){
        let categories = []
        this.state.categories.forEach((c, _i) => {
            categories.push(
                (<ProductCat info={c} key={c.cat} products={c.products} title={c.cat}/>)
            )
        });
        return (
            <div className="categories-list-div">
                <ul className="categories-list-ul">
                    {categories}
                </ul>
            </div>
        )

    }
    render(){
        const cats = this.displayer();
        return(
            <div className="product-panel">
                <div className="page-title">
                    <span>Products</span>
                </div>
                {cats}
            </div>
        )
    }
}

export default ProductPanel
import React, {Component} from 'react';
import GetQuote from './GetQuote.js';
import './ProductFigure.css';

class ProductFigure extends Component{
    constructor(props){
        super(props);
        this.firstClick = this.firstClick.bind(this);
        this.state = {show: false, sent: false};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.makeBlue = this.makeBlue.bind(this);
        this.changeColor = this.changeColor.bind(this);
    }
    firstClick(e){
        //when a user clicks the button, the "sent" attribute is set to false
        //this is done to reset the state of the component
        return this.setState({show: true, sent: false});
    };
    handleSubmit(formData){
        console.log(formData);
        this.sendEmail(formData);
        return this.setState({show: false, sent: true});
    };
    async sendEmail(data){
        let formData = JSON.stringify({...data, product: this.props.info.name});
        console.log(formData);
        console.log({...data, product: this.props.info.name});
       const options = {method: "POST", headers: {'Content-Type': 'application/json;charset=utf-8'}, 
                body: formData};
        fetch('https://luvabachemicals.herokuapp.com/api/post/quote', options)
        .then(res => res.json())
        .then(res => {
            if(res.ok){
                console.log('hooray');
            } else{
                console.log(res);
                console.log(res.errors)
            }
        })
    };
    makeBlue(e){
        let target = e.target.closest('.product-figure-container');
        target.style.backgroundColor = "rgb(143, 226, 215)";
    };
    changeColor(e){
        let target = e.target.closest('.product-figure-container');
        target.style.backgroundColor = "rgba(255, 245, 238, 0.473)";
    }
    render(){
        const {name, description, image, imageDescription} = this.props.info;
        console.log(image);
        //show will be different based on whether a user has clicked "get quote" or "finish"
        let afterSent = (
            <div className="after-sent-container">
                <div className="after-sent-text">
                    <span>
                        Thank you for your enquiry. A quote will be shortly sent to your inbox
                    </span>
                </div>
                <div className="button-div">
                    <button type="button" onClick={this.firstClick}>Get Quote</button>
                </div>
            </div>
        )
        let show = (this.state.show ? (<GetQuote handleSubmit={this.handleSubmit} />) : (this.state.sent ? 
            (afterSent): (<div className="button-div">
            <button type="button" onClick={this.firstClick}>Get Quote</button>
        </div>)));
        console.log(show);
        return (
            <div className="product-figure-container" onMouseOver={this.makeBlue} onMouseOut={this.changeColor}>
                <div className="product-wrapper">
                    <ul>
                        <li className="image-li">
                            <img src={image} alt={imageDescription}></img>
                        </li>
                        <li className="product-info-li">
                            <span className="product-name">
                                {name}
                            </span>
                            <span className="product-desc">
                               . {description}
                            </span>
                        </li>
                    </ul>
                </div>
                    {show}
            </div>
        )
    }
}

export default ProductFigure;
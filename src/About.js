import React, {Component} from 'react';
import './About.css'

class About extends Component{
    render(){
        return (
            <div className="about-container">
                <div className="first-container">
                    <ul>
                        <li className="first-title">
                            <h3>About Us</h3>
                        </li>
                        <li className="first-para">
                            <p>Luvaba Chemical Solutions tradings as Luvaba is a hygiene and cleaning chemicals 
                                business situated in a small town called Whittlesea in the Eastern Cape
                                province of South Africa. The business is owned by Mr Luvo Ntsangani who is an
                                experienced product development scientist with a Master of Science in Chemistry
                                from the Univiversity of the Witwatersrand coupled with 10 years working experience
                                in chemical manufacturing industry. Luvo has exceptional experience in developing,
                                 validating, commercialising and marketing hygiene and cleaning chemicals as he has
                                 worked for big companies like Unilever, Henkel and AECI. The business is co-owned by 
                                 Mr Xolisile Vankie Ntsangani who has a grade 12 certificate from John Noah Senior 
                                 Secondary School coupled with 15 years working experience as a sales representative 
                                 and warehouse manager in the procurement and logistics industry.
                            </p>
                        </li>
                    </ul>
                </div>
                <div className="second-container">
                    <ul>
                        <li className="second-title">
                            <h3>Purpose Of The Business</h3>
                        </li>
                        <li className="second-para">
                            <p>
                                The puropse of the business is to develop, manufacture, package, and sell hygiene and 
                                cleaning chemicals to the community in which it operates and to government, retail and industrial
                                 establishments around the Eastern Cape province. One of the objectives of the business is to 
                                 eradicate unhygienic and dirty environments at a personal level and at the homes and industries 
                                 of the community and province in which we operate. Another objective is to be the chosen supplier of hygiene 
                                 and cleaning chemicals by the community, government and businesses around the Eastern Cape.
                            </p>
                            <br />
                            <p>
                                We manufacture our products using quality raw materials sourced from reliable
                                local suppliers and ensure that the raw materials are ethically produced in order
                                to benefit society and the environment. At Luvaba Chemical Solutions we know that 
                                products that harm the enviroment do not serve the community, which is why we
                                continuously audit our supply chain to ensure it is in accord with the National
                                Management Acts of South Africa.
                            </p>
                            <br />
                            <p>
                                Our customers are the lifeblood of our organisation, which is why we are offering free
                                 samples to be tested by our customers. On our website you will find quality products
                                 for every need. Feel free to give us a call or browse our product catalogue.
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        )
    }
}

export default About;
import React, {Component} from 'react';
import ContactForm from './ContactForm.js';
import './Contact.css';

class Contact extends Component{
    constructor(props){
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {sent: false}
    };
    onSubmit(data){
        console.log(data);
        this.sendEmail(data)
        return this.setState({sent: true});
    };
    async sendEmail(data){
        const options = {method: "POST", headers: {'Content-Type': 'application/json;charset=utf-8'}, 
                 body: JSON.stringify(data)};
         fetch('https://luvabachemicals.herokuapp.com/api/post/enquiry', options)
         .then(res => res.json())
         .then(res => {
             if(res.ok){
                 console.log('hooray');
             }
         })
     };
    render(){
        let afterSent = (
            <div className="after-sent-container">
                <div className="after-sent-text">
                    <span>
                        Thank you for your enquiry. We will get back to you shortly
                    </span>
                </div>
            </div>
        );
        let show = (
            (this.state.sent) ? (afterSent) : (<ContactForm onSubmit={this.onSubmit} />)
        )
        return (
            <div className="contact-panel">
                <div className="contact-info-container">
                    <div className="line-header">
                        Details
                    </div>
                    <div className="top-info">
                        <div className="other-info">
                            Business registration number: 2020/614414/07
                            <br/>
                            Enterprise Name: LUVABA CHEMICAL SOLUTIONS
                            <br />
                            Managing Director: Mr Luvo Ntsangani
                            <br />
                            Contact Number: <span className="contact-number">0848710043</span>
                            <br />
                        </div>
                        <div className="bottom-info">
                            <div className="bottom-left-div">
                                Business Operations
                            </div>
                            <div className="bottom-right-div">
                                840B Sada Township
                                <br />
                                Whittlesea
                                <br />
                                Eastern Cape
                                <br />
                                5360
                            </div>
                        </div>
                    </div>
                </div>
                <div className="contact-form-panel">
                    <div className="line-header">
                        Enquire
                    </div>
                    <div className="show-container">
                        {show}
                    </div>
                </div>
            </div>
        )
    }
}

export default Contact;
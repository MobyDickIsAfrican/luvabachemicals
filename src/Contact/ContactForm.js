import React, {Component} from 'react';

class ContactForm extends Component{
    constructor(props){
        super(props);
        this.onSubmit = this.onSubmit.bind(this);
    };
    onSubmit(e){
        e.preventDefault();
        const fullName = e.target.fullname.value;
        const email = e.target.email.value; 
        const number = e.target.cell.value;
        const message = e.target.msg.value;
        return this.props.onSubmit({fullName, email, number, message})
    }
    render(){
        return (
            <div className="contact-form-container">
                <form onSubmit={this.onSubmit}>
                    <div className="fullname-div">
                        <ul>
                            <li className="fullname-label">
                                Fullname
                            </li>
                            <li className="fullname-input-li">
                                <input type="text" name="fullname"></input>
                            </li>
                        </ul>
                    </div>
                    <div className="email-div">
                        <ul>
                            <li className="email-label">
                                Email
                            </li>
                            <li className="email-input-li">
                                <input type="email" name="email"></input>
                            </li>
                        </ul>
                    </div>
                    <div className="cell-div">
                        <ul>
                            <li className="cell-label">
                                Cell
                            </li>
                            <li className="cell-input-li">
                                <input type="tel" name="cell"></input>
                            </li>
                        </ul>
                    </div>
                    <div className="msg-div">
                        <ul>
                            <li className="msg-label">
                                Message
                            </li>
                            <li className="msg-input-li">
                                <textarea type="text" name="msg"></textarea>
                            </li>
                        </ul>
                    </div>
                    <div className="contact-submit">
                        <button type="submit">Send</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default ContactForm;
import React, {Component} from 'react';
import HomeProducts from './Home Page/Home Products/HomeProducts.js';
import Hero from './Home Page/Hero.js';
import HomeOne from './Home Page/HomeOne.js';
import HomeTwo from './Home Page/HomeTwo.js';
import WeServe from './Home Page/We Serve/WeServe.js';
import OurFounderContainer from './Home Page/OurFounder/OurFounderContainer.js';
import CallToAction from './Home Page/CallToAction.js';

class Home extends Component{
    render(){
        return (
            <div className="home-container">
                <Hero />
                <HomeOne />
                <HomeTwo />
                <HomeProducts />
                <WeServe />
                <OurFounderContainer />
                <CallToAction />
            </div>
        )
    }
}

export default Home;